<?php

class Usuario_Controller extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->checkAccess('super');

        $this->load->view('layout/header');
        $this->load->view('layout/nav');
        $this->load->view('usuario/index');
        $this->load->view('layout/footer');
    }

    public function view($id = '') {
        $this->checkAccess('normal');

        if (!empty(filter_input(INPUT_GET, 'id'))) {
            $id = filter_input(INPUT_GET, 'id');
        }
        $this->checkManage($id);

        $data['user'] = $this->aauth->get_user($id);
        $data['id'] = $id;
        $data['admin'] = false;
        if ($this->aauth->is_admin($id)) {
            $data['admin'] = true;
        }
        if (!empty($data['user']->birthday)) {
            $date = $data['user']->birthday;
            $data['birthday'] = $this->formatDate($date);
        }

        $this->load->view('layout/header');
        $this->load->view('layout/nav');
        $this->load->view('usuario/view', $data);
        $this->load->view('layout/footer');
    }

    public function search() {
        $this->checkAccess('super');

        $this->load->view('layout/header');
        $this->load->view('layout/nav');
        $this->load->view('usuario/search');
        $this->load->view('layout/footer');
    }

    public function all() {
        $this->checkAccess('super');

        $this->load->model('usuario');
        $data['users'] = $this->usuario->get_all();

        $this->load->view('layout/header');
        $this->load->view('layout/nav');
        $this->load->view('usuario/list', $data);
        $this->load->view('layout/footer');
    }

    public function insert() {
        $data = array();
        $data['email'] = $this->input->post('mail');
        $data['pass'] = $this->input->post('pass');
        $data['name'] = $this->input->post('nome');
        $data['username'] = $this->input->post('username');
        $data['gender'] = $this->input->post('sexo');
        $data['birthday'] = $this->input->post('dtnasc');
        $data['tel'] = $this->input->post('fone');
        $data['location'] = $this->input->post('endereco');

        if (!empty((filter_input(INPUT_POST, 'admin')))) {
            $data['admin'] = true;
        } else {
            $data['admin'] = false;
        }

        if (!empty((filter_input(INPUT_POST, 'fb')))) {
            $data['fb_access'] = true;
        } else {
            $data['fb_access'] = false;
        }

        if ($this->aauth->check_email($data['email'])) {
            if ($this->aauth->create_user($data)) {
                redirect('auth');
            } else {
                print_r($this->aauth->get_errors_array());
            }
        } else {
            $message = $this->aauth->get_errors_array();
            echo "<script>";
            echo "alert('$message');";
            echo "location.href='../auth/signup'";
            echo "</script>";
        }
    }

    public function find() {
        $this->checkAccess('super');

        $mail = $this->input->post('mail');
        $id = $this->aauth->get_user_id($mail);

        if (!is_null($id)) {
            $this->view($id);
        } else {
            $message = 'Nada encontrado';
            echo "<script>";
            echo "alert('$message');";
            echo "location.href='index'";
            echo "</script>";
        }
    }

    public function delete($id = '') {
        $this->checkAccess('normal');

        if (!empty(filter_input(INPUT_GET, 'id'))) {
            $id = filter_input(INPUT_GET, 'id');
        }
        $this->checkManage($id);

        $user = $this->aauth->get_user();
        if (!$this->aauth->is_admin($user->id)) {
            $this->aauth->logout();
        }
        $this->aauth->delete_user($id);
        redirect('usuario_controller');
    }

    public function edit($id = '') {
        $this->checkAccess('normal');

        if (!empty(filter_input(INPUT_GET, 'id'))) {
            $id = filter_input(INPUT_GET, 'id');
        }
        $this->checkManage($id);

        $data['user'] = $this->aauth->get_user($id);
        $data['id'] = $id;
        $data['admin'] = false;
        if ($this->aauth->is_admin($id)) {
            $data['admin'] = true;
        }
        $data['birthday'] = $data['user']->birthday;
        if (!empty($data['user']->birthday)) {
            $date = $data['user']->birthday;
            $data['birthday'] = $this->formatDate($date);
        }
        $data['fb'] = false;
        if ($data['user']->fb_access == 1) {
            $data['fb'] = true;
        }

        $this->load->view('layout/header');
        $this->load->view('layout/nav');
        $this->load->view('usuario/edit', $data);
        $this->load->view('layout/footer');
    }

    public function update() {
        $this->checkAccess('normal');

        $id = filter_input(INPUT_POST, 'id');
        $data = array();
        $data['email'] = $this->input->post('mail');
        $data['name'] = $this->input->post('nome');
        $data['username'] = $this->input->post('username');
        $data['gender'] = $this->input->post('sexo');
        $data['birthday'] = $this->input->post('dtnasc');
        $data['tel'] = $this->input->post('fone');
        $data['location'] = $this->input->post('endereco');

        if (!empty((filter_input(INPUT_POST, 'admin')))) {
            $data['admin'] = true;
        } else {
            $data['admin'] = false;
        }

        if (!empty((filter_input(INPUT_POST, 'fb_access')))) {
            $data['fb_access'] = true;
        } else {
            $data['fb_access'] = false;
        }

        if ($this->aauth->update_user($id, $data)) {
            redirect("usuario_controller/view?id=$id");
        } else {
            print_r($this->aauth->get_errors_array());
        }
    }

    public function password($id = '') {
        $this->checkAccess('normal');

        if (!empty(filter_input(INPUT_GET, 'id'))) {
            $id = filter_input(INPUT_GET, 'id');
        }
        $this->checkManage($id);

        $data['user'] = $this->aauth->get_user($id);
        $data['id'] = $id;

        $this->load->view('layout/header');
        $this->load->view('layout/nav');
        $this->load->view('usuario/password', $data);
        $this->load->view('layout/footer');
    }

    public function changePassword() {
        $this->checkAccess('normal');

        $id = $this->input->post('id');
        $pass1 = $this->input->post('pass1');
        $pass2 = $this->input->post('pass2');

        if (strcmp($pass1, $pass2) == 0 &&
                $this->aauth->change_password($id, $pass1)) {
            redirect("usuario_controller/view?id=$id");
        } else {
            echo 'erro';
        }
    }

    private function formatDate($dt) {
        $date = DateTime::createFromFormat('Y-m-d', $dt);
        return $date->format('d/m/Y');
    }

}
