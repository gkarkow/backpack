<?php

class Foto_Controller extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index($idlocal = '') {
        $this->checkAccess('normal');

        if (!empty(filter_input(INPUT_GET, 'idlocal'))) {
            $idlocal = filter_input(INPUT_GET, 'idlocal');
        }

        $data['idlocal'] = $idlocal;

        $this->load->view('layout/header');
        $this->load->view('layout/nav');
        $this->load->view('fotos', $data);
        $this->load->view('layout/footer');
    }

    public function add_foto() {
        $this->load->model('foto');
        $config = $this->config_foto();
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {
            $error = array('error' => $this->upload->display - errors());
        } else {
            $data = array('upload_data' => $this->upload->data());
            $dados['foto'] = $data['upload_data']['file_name'];
            $idlocal = $this->input->post('idlocal');
            $dados['local_idlocal'] = $idlocal;
            $dados['usuario_idusuario'] = $this->aauth->get_user_id();
            if ($this->foto->insert($dados)) {
                redirect("local_controller/view?id=$idlocal");
            }
        }
    }

    private function config_foto() {
        $config['upload_path'] = './upload_fotos/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '0';
        $config['max_width'] = '0';
        $config['max_height'] = '0';
        return $config;
    }

}
