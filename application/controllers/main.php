<?php

class Main extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index($destino = '') {
        if (!empty(filter_input(INPUT_GET, 'destino'))) {
            $destino = filter_input(INPUT_GET, 'destino');
        } else {
            $destino = $this->input->post('destino');
        }
        $latlng = $this->get_latlng($destino);
        $places = $this->get_markers($destino);
        $i = 0;
        $markers = array();
        $locais = array();
        foreach ($places as $place) {
            $markers[$i]['id'] = $place->idlocal;
            $markers[$i]['long'] = $place->coordx;
            $markers[$i]['lat'] = $place->coordy;
            $markers[$i]['name'] = $place->nome;
            $markers[$i]['type'] = $this->get_type($place->tipo_local_idtipo_local);
            $i++;

            $ratings = $this->get_ratings($place->idlocal);
            $locais[] = array(
                'id' => $place->idlocal,
                'nome' => $place->nome,
                'tipo' => $this->get_type($place->tipo_local_idtipo_local),
                'nota' => $ratings['rate_bg']
            );
        }
        usort($locais, $this->make_comparer(['nota', SORT_DESC], ['nome']));
        $data['locais'] = array_slice($locais, 0, 5);
        $data['lat'] = $latlng['lat'];
        $data['lng'] = $latlng['lng'];
        $data['markers'] = $markers;
        $data['destino'] = $destino;

        $this->load->view('layout/header');
        $this->load->view('layout/nav_map');
        $this->load->view('map', $data);
        $this->load->view('layout/footer');
    }

    private function get_markers($destino) {
        return $this->map->get_all_coordinates($destino, '');
    }

    private function get_latlng($destino) {
        $address = str_replace(" ", "+", $destino);
        $geocode_stats = file_get_contents("http://maps.googleapis.com/maps/api/geocode/json?address=$address&sensor=false");
        $output_deals = json_decode($geocode_stats);
        $latLng = $output_deals->results[0]->geometry->location;

        $data['lat'] = $latLng->lat;
        $data['lng'] = $latLng->lng;
        return $data;
    }

    private function get_type($idtipo_local) {
        switch ($idtipo_local) {
            case 1: return 'hotel';
            case 2: return 'noite';
            case 3: return 'comida';
            case 4: return 'turismo';
            default: return '';
        }
    }

    private function get_ratings($idlocal) {
        $this->load->model('rate');
        $ratings = $this->rate->get_by_local($idlocal);
        $rate_db = array();
        foreach ($ratings as $r) {
            $rate_db[] = $r;
            $sum_rates[] = $r->rate;
        }
        if (count($rate_db)) {
            $rate_times = count($rate_db);
            $sum_rates = array_sum($sum_rates);
            $rate_value = $sum_rates / $rate_times;
            $rate_bg = (($rate_value) / 5) * 100;
        } else {
            $rate_times = 0;
            $rate_value = 0;
            $rate_bg = 0;
        }
        return array(
            'rate_times' => $rate_times,
            'rate_value' => $rate_value,
            'rate_bg' => $rate_bg
        );
    }

    private function make_comparer() {
        // Normalize criteria up front so that the comparer finds everything tidy
        $criteria = func_get_args();
        foreach ($criteria as $index => $criterion) {
            $criteria[$index] = is_array($criterion) ? array_pad($criterion, 3, null) : array($criterion, SORT_ASC, null);
        }

        return function($first, $second) use (&$criteria) {
            foreach ($criteria as $criterion) {
                // How will we compare this round?
                list($column, $sortOrder, $projection) = $criterion;
                $sortOrder = $sortOrder === SORT_DESC ? -1 : 1;

                // If a projection was defined project the values now
                if ($projection) {
                    $lhs = call_user_func($projection, $first[$column]);
                    $rhs = call_user_func($projection, $second[$column]);
                } else {
                    $lhs = $first[$column];
                    $rhs = $second[$column];
                }

                // Do the actual comparison; do not return if equal
                if ($lhs < $rhs) {
                    return -1 * $sortOrder;
                } else if ($lhs > $rhs) {
                    return 1 * $sortOrder;
                }
            }

            return 0; // tiebreakers exhausted, so $first == $second
        };
    }

}
