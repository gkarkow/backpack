<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Welcome extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('layout/header');
        $this->load->view('layout/nav_index');
        $this->load->view('welcome_message');
        $this->load->view('layout/footer');
    }

}
