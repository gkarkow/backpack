<?php

define('DISQUS_SECRET_KEY', 'UtAGbty7Jyg8IZEG9iSt8fqMTpM0Yu1m1pV2eJhPmy2yN188PC3cwosbTVAT2SNB');
define('DISQUS_PUBLIC_KEY', 'Wcj9Ynq2Ki91ExCbpGtjUMF2dD8ciA4tq3qB5ptWK6dDQQZlogCnfSdrwYHUVdGh');

class Local_Controller extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index($tipolocal = '') {
        $this->checkAccess('super');

        if (!empty(filter_input(INPUT_GET, 'tipolocal'))) {
            $tipolocal = filter_input(INPUT_GET, 'tipolocal');
        }
        $data['tipolocal'] = $tipolocal;

        $this->load->view('layout/header');
        $this->load->view('layout/nav');
        $this->load->view('local/index', $data);
        $this->load->view('layout/footer');
    }

    public function view($id = '') {
        if (!empty(filter_input(INPUT_GET, 'id'))) {
            $id = filter_input(INPUT_GET, 'id');
        }

        $this->load->model('local');
        $local = $this->local->get_by_id($id);
        $data['local'] = $local[0];
        $data['id'] = $id;
        $destino = ucwords(strtolower($this->localizacao($local[0]->cidade_cod_cidade)));
        $data['destino'] = $destino;
        $this->load->model('tipo_local');
        $tipo_local = ucwords(strtolower($this->tipo_local->retorna_nome($local[0]->tipo_local_idtipo_local)));
        $data['tipo_local'] = $tipo_local;
        $data['type'] = $local[0]->tipo_local_idtipo_local;

        $this->load->model('foto');
        $photos = $this->foto->get_all_by_local($id);
        if (empty($photos)) {
            $photos = false;
        }
        $data['photos'] = $photos;

        // DISQUS
        if ($this->aauth->is_loggedin()) {
            $u = $this->aauth->get_user();
            $userdata = array(
                'id' => $u->id,
                'username' => $u->username,
                'email' => $u->email
            );
            $message = base64_encode(json_encode($userdata));
            $timestamp = time();
            $hmac = $this->dsq_hmacsha1($message . ' ' . $timestamp, DISQUS_SECRET_KEY);
            $data['message'] = $message;
            $data['timestamp'] = $timestamp;
            $data['hmac'] = $hmac;
            $data['publickey'] = DISQUS_PUBLIC_KEY;
        }
        //
        // Rating
        $this->load->model('rate');
        $ratings = $this->rate->get_by_local($id);
        $rate_db = array();
        foreach ($ratings as $r) {
            $rate_db[] = $r;
            $sum_rates[] = $r->rate;
        }
        if (count($rate_db)) {
            $rate_times = count($rate_db);
            $sum_rates = array_sum($sum_rates);
            $rate_value = $sum_rates / $rate_times;
            $rate_bg = (($rate_value) / 5) * 100;
        } else {
            $rate_times = 0;
            $rate_value = 0;
            $rate_bg = 0;
        }
        $data['rate_times'] = $rate_times;
        $data['rate_value'] = $rate_value;
        $data['rate_bg'] = $rate_bg;
        //

        $this->load->view('layout/header');
        $this->load->view('layout/nav');
        $this->load->view('local/view', $data);
        $this->load->view('layout/footer');
    }

    public function search($tipolocal = '') {
        $this->checkAccess('super');

        if (!empty(filter_input(INPUT_GET, 'tipolocal'))) {
            $tipolocal = filter_input(INPUT_GET, 'tipolocal');
        }

        $data['tipolocal'] = $tipolocal;

        $this->load->view('layout/header');
        $this->load->view('layout/nav');
        $this->load->view('local/search', $data);
        $this->load->view('layout/footer');
    }

    public function all($tipolocal = '') {
        $this->checkAccess('super');

        if (!empty(filter_input(INPUT_GET, 'tipolocal'))) {
            $tipolocal = filter_input(INPUT_GET, 'tipolocal');
        }

        $this->load->model('local');
        $dados['local'] = $this->local->get_all_type($tipolocal);
        $this->load->view('layout/header');
        $this->load->view('layout/nav');
        $this->load->view('local/list', $dados);
        $this->load->view('layout/footer');
    }

    public function create($tipolocal = '') {
        $this->checkAccess('super');

        if (!empty(filter_input(INPUT_GET, 'tipolocal'))) {
            $tipolocal = filter_input(INPUT_GET, 'tipolocal');
        }

        $this->load->model(array('estado', 'tipo_local'));

        $dados = array();
        $dados['estados'] = $this->estado->get_all();
        $dados['locais'] = $this->tipo_local->get_all();
        $dados['tipolocal'] = $tipolocal;

        $this->load->view('layout/header');
        $this->load->view('layout/nav');
        $this->load->view('local/insere_local', $dados);
        $this->load->view('layout/footer');
    }

    public function insert() {
        $this->checkAccess('super');
        $this->load->model('local');

        $dados = array(
            'cidade_cod_cidade' => $this->input->post('cidade'),
            'tipo_local_idtipo_local' => $this->input->post('idtipo_local'),
            'nome' => $this->input->post('nome'),
            'coordx' => $this->input->post('coordx'),
            'coordy' => $this->input->post('coordy')
        );

        if ($this->local->insert($dados)) {
            redirect('local_controller');
        }
    }

    public function find() {
        $this->checkAccess('super');

        $nome = $this->input->post('nomelocal');
        $tipo = $this->input->post('tipolocal');

        $this->load->model('local');
        $loc = $this->local->get_by_name_and_type($nome, $tipo);
        if (!empty($loc)) {
            $this->view($loc[0]->idlocal);
        } else {
            $message = 'Nada encontrado';
            echo "<script>";
            echo "alert('$message');";
            echo "location.href='search?tipolocal=$tipo'";
            echo "</script>";
        }
    }

    public function delete($id = '', $type = '') {
        $this->checkAccess('super');

        if (!empty(filter_input(INPUT_GET, 'id'))) {
            $id = filter_input(INPUT_GET, 'id');
        }
        if (!empty(filter_input(INPUT_GET, 'type'))) {
            $type = filter_input(INPUT_GET, 'type');
        }

        $this->load->model('local');
        if ($this->local->delete($id)) {
            redirect("local_controller?tipolocal=$type");
        }
    }

    public function edit($id = '') {
        $this->checkAccess('super');
        $this->load->model(array('local', 'cidade', 'estado'));
        if (!empty(filter_input(INPUT_GET, 'id'))) {
            $id = filter_input(INPUT_GET, 'id');
        }

        $local = $this->local->get_by_id($id);
        $data['estados'] = $this->estado->get_all();
        $c = $this->cidade->get_by_id($local[0]->cidade_cod_cidade);
        $data['c'] = $c[0];
        $e = $this->estado->get_by_id($c[0]->estado_cod_estado);
        $data['e'] = $e[0];
        $data['local'] = $local[0];
        $this->load->view('layout/header');
        $this->load->view('layout/nav');
        $this->load->view('local/edit', $data);
        $this->load->view('layout/footer');
    }

    public function update() {
        $this->checkAccess('super');
        $this->load->model('local');
        $id = $this->input->post('idlocal');

        $dados = array(
            'cidade_cod_cidade' => $this->input->post('cidade'),
            'tipo_local_idtipo_local' => $this->input->post('tipo'),
            'nome' => $this->input->post('nome'),
            'coordx' => $this->input->post('coordx'),
            'coordy' => $this->input->post('coordy')
        );

        if ($this->local->update($id, $dados)) {
            redirect("local_controller/view?id=$id");
        }
    }

    public function chama_cidades() {
        $this->load->model('cidade');

        $estado = $this->input->post('estado');

        $localidades = $this->cidade->localidades($estado);

        echo "<script>" . "$('#cidade').selectpicker();" . "</script>";
        echo "<select class='selectpicker' data-width='100%' name='cidade' id='cidade'>";
        foreach ($localidades as $fila) {
            echo "<option value='$fila->cod_cidade'>$fila->nome</option>";
        }
        echo "</select>";
    }

    public function rating() {
        $this->checkAccess('normal');
        $this->load->model('rate');

        $rate = $this->input->post('rate');
        $id_user = $this->aauth->get_user_id();
        $id_local = $this->input->post('idlocal');

        $voted = $this->rate->already_voted($id_user, $id_local);

        if (!$voted) {
            $data = array(
                'rate' => $rate,
                'id_user' => $id_user,
                'id_local' => $id_local
            );
            $this->rate->insert($data);
        } else {
            $data = array(
                'rate' => $rate
            );
            $this->rate->update($id_user, $id_local, $data);
        }
    }

    private function localizacao($cod_cidade) {
        $destino = '';
        $this->load->model('cidade');
        $cidade = $this->cidade->get_by_id($cod_cidade);
        $destino .= $cidade[0]->nome;
        $this->load->model('estado');
        $estado = $this->estado->get_by_id($cidade[0]->estado_cod_estado);
        $destino .= ", ";
        $destino .= $estado[0]->nome;
        return $destino;
    }

    private function dsq_hmacsha1($data, $key) {
        $blocksize = 64;
        $hashfunc = 'sha1';
        if (strlen($key) > $blocksize) {
            $key = pack('H*', $hashfunc($key));
        }
        $key = str_pad($key, $blocksize, chr(0x00));
        $ipad = str_repeat(chr(0x36), $blocksize);
        $opad = str_repeat(chr(0x5c), $blocksize);
        $hmac = pack(
                'H*', $hashfunc(
                        ($key ^ $opad) . pack(
                                'H*', $hashfunc(
                                        ($key ^ $ipad) . $data
                                )
                        )
                )
        );
        return bin2hex($hmac);
    }

}
