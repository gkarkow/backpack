<?php

class Cidade_Controller extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->checkAccess('super');

        $this->load->view('layout/header');
        $this->load->view('layout/nav');
        $this->load->view('cidade/index');
        $this->load->view('layout/footer');
    }

    public function create() {
        $this->checkAccess('super');

        $this->load->model('estado');
        $dados['estados'] = $this->estado->get_all();

        $this->load->view('layout/header');
        $this->load->view('layout/nav');
        $this->load->view('cidade/insert', $dados);
        $this->load->view('layout/footer');
    }

    public function insert() {
        $this->checkAccess('super');

        $this->load->model('cidade');
        $dados = array(
            'estado_cod_estado' => $this->input->post('estado'),
            'nome' => $this->input->post('nome'),
            'cep' => $this->input->post('cep')
        );

        if ($this->cidade->insert($dados)) {
            redirect('cidade_controller');
        }
    }

    public function search() {
        $this->checkAccess('super');

        $this->load->view('layout/header');
        $this->load->view('layout/nav');
        $this->load->view('cidade/search');
        $this->load->view('layout/footer');
    }

    //pode encontrar mais de uma cidade 
    public function find() {
        $this->checkAccess('super');

        $nomecidade = $this->input->post('nomecidade');
        $this->load->model('cidade');
        $this->load->model('estado');
        $cid = $this->cidade->get_by_name($nomecidade);
        if (!empty($cid)) {
            if (!empty($cid[1])) {
                foreach ($cid as $c) {
                    $e = $this->estado->get_by_id($c->estado_cod_estado);
                    $c->estado_cod_estado = $e[0]->sigla;
                }
                $this->result($cid);
            } else {
                $this->view($cid[0]->cod_cidade);
            }
        } else {
            $message = 'Nada encontrado';
            echo "<script>";
            echo "alert('$message');";
            echo "location.href='index'";
            echo "</script>";
        }
    }

    public function edit($id = '') {
        if (!empty(filter_input(INPUT_GET, 'id'))) {
            $id = filter_input(INPUT_GET, 'id');
        }

        $this->load->model(array('cidade', 'estado'));
        $cidade = $this->cidade->get_by_id($id);
        $dados['cidade'] = $cidade[0];
        $dados['estados'] = $this->estado->get_all();

        $this->load->view('layout/header');
        $this->load->view('layout/nav');
        $this->load->view('cidade/edit', $dados);
        $this->load->view('layout/footer');
    }

    public function update() {
        $this->load->model('cidade');
        $idC = $this->input->post('id');
        $dados = array(
            'estado_cod_estado' => $this->input->post('estado'),
            'nome' => $this->input->post('nome'),
            'cep' => $this->input->post('cep')
        );

        if ($this->cidade->update($idC, $dados)) {
            redirect("cidade_controller/view?id=$idC");
        }
    }

    public function delete($id = '') {
        $this->checkAccess('super');

        if (!empty(filter_input(INPUT_GET, 'id'))) {
            $id = filter_input(INPUT_GET, 'id');
        }

        $this->load->model('cidade');
        if ($this->cidade->delete($id)) {
            redirect("cidade_controller");
        }
    }

    public function view($id = '') {
        $this->checkAccess('super');

        if (!empty(filter_input(INPUT_GET, 'id'))) {
            $id = filter_input(INPUT_GET, 'id');
        }

        $this->load->model('cidade');
        $this->load->model('estado');
        $cid = $this->cidade->get_by_id($id);
        $dados['id'] = $cid[0]->cod_cidade;
        $dados['nome'] = $cid[0]->nome;
        $dados['cep'] = $cid[0]->cep;
        $e = $this->estado->get_by_id($cid[0]->estado_cod_estado);
        $dados['estado'] = $e[0]->nome . " - " . $e[0]->sigla;
        $destino = "" . $cid[0]->nome . ", " . $e[0]->nome . "";
        $dados['destino'] = $destino;

        $this->load->view('layout/header');
        $this->load->view('layout/nav');
        $this->load->view('cidade/view', $dados);
        $this->load->view('layout/footer');
    }

    public function all() {
        $this->checkAccess('super');

        $estado = $this->input->post('estado');
        $this->load->model('cidade');
        $dados['cidades'] = $this->cidade->localidades($estado);

        $this->load->view('layout/header');
        $this->load->view('layout/nav');
        $this->load->view('cidade/list', $dados);
        $this->load->view('layout/footer');
    }

    public function result($cidades) {
        $this->checkAccess('super');

        $data['cidades'] = $cidades;

        $this->load->view('layout/header');
        $this->load->view('layout/nav');
        $this->load->view('cidade/result', $data);
        $this->load->view('layout/footer');
    }

    public function state() {
        $this->checkAccess('super');

        $this->load->model('estado');
        $dados = array();
        $dados['estados'] = $this->estado->get_all();

        $this->load->view('layout/header');
        $this->load->view('layout/nav');
        $this->load->view('cidade/state', $dados);
        $this->load->view('layout/footer');
    }

}
