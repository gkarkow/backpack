<?php

class Passagem extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index($destino = '') {
        if (!empty(filter_input(INPUT_GET, 'destino'))) {
            $destino = filter_input(INPUT_GET, 'destino');
        }
        $data['destino'] = $destino;

        $this->load->view('layout/header');
        $this->load->view('layout/nav');
        $this->load->view('passagem', $data);
        $this->load->view('layout/footer');
    }

    public function go() {
        $o = explode(",", $this->input->post('origem'));
        $d = explode(",", $this->input->post('destino'));
        $origem = str_replace(" ", "%20", $o[0]);
        $destino = str_replace(" ", "%20", $d[0]);
        $data = $this->input->post('data');
        $url = "http://www.submarinoviagens.com.br/passagens/selecionarvoo?SomenteIda=true&Origem=$origem&Destino=$destino&Data=$data&NumADT=1&NumCHD=0&NumINF=0&SomenteDireto=false&Hora=&Hora=&selCabin=&Multi=false&Cia=&AffiliatedID=&s_cid=&utm_medium=&utm_source=&utm_campaign=";
        redirect($url);
    }

}
