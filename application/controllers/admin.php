<?php

class Admin extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->checkAccess('super');
        
        $admin = $this->num_users(1);
        $users = $this->num_users(3);
        
        $data['admin'] = $admin;
        $data['users'] = $users;

        $this->load->view('layout/header');
        $this->load->view('layout/nav');
        $this->load->view('admin/index', $data);
        $this->load->view('layout/footer');
    }
    
    private function num_users($group_id) {
        $users = $this->aauth->list_users($group_id);
        $i = array();
        foreach ($users as $user) {
            $i[] = $user;
        }
        return count($i);
    }

}
