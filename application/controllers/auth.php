<?php

class Auth extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function signin() {
        $this->checkSession();

        $this->load->view('layout/header');
        $this->load->view('layout/nav');
        $this->load->view('auth/signin');
        $this->load->view('layout/footer');
    }

    public function signup() {
        if ($this->aauth->is_loggedin() && !$this->aauth->is_allowed('super')) {
            redirect(base_url());
        }

        ///////////////////////////////////////////////////////////////////
        $user = $this->facebook->getUser();
        $data['user_profile'] = false;
        if ($user) {
            try {
                $data['user_profile'] = $this->facebook->api('/me');
                $date = $data['user_profile']['birthday'];
                $data['user_profile']['birthday'] = $this->formatDate($date);
            } catch (FacebookApiException $e) {
                $user = null;
            }
        } else {
            $this->facebook->destroySession();
            $data['login_url'] = $this->facebook->getLoginUrl(array(
                'redirect_uri' => site_url('auth/signup'),
                'scope' => array('email', 'user_birthday', 'user_location')
            ));
        }
        ///////////////////////////////////////////////////////////////////

        $this->load->view('layout/header');
        $this->load->view('layout/nav');
        $this->load->view('auth/signup', $data);
        $this->load->view('layout/footer');

        if ($user) {
            $this->facebook->destroySession();
        }
    }

    public function login() {
        $mail = $this->input->post('mail');
        $pass = $this->input->post('pass');
        if ($this->aauth->login($mail, $pass)) {
            redirect('auth');
        } else {
            $errors = reset($this->aauth->get_errors_array());
            $message = $errors;
            echo "<script>";
            echo "alert('$message');";
            echo "location.href='signin'";
            echo "</script>";
        }
    }

    public function logout() {
        $this->aauth->logout();
        $this->facebook->destroySession();
        redirect(base_url());
    }

    public function index() {
        $this->checkSession();

        $user = $this->facebook->getUser();
        $data['user_profile'] = false;

        if ($user) {
            try {
                $data['user_profile'] = $this->facebook->api('/me');
            } catch (FacebookApiException $e) {
                $user = null;
            }
        } else {
            $this->facebook->destroySession();
            $data['login_url'] = $this->facebook->getLoginUrl(array(
                'redirect_uri' => site_url('auth'),
                'scope' => array('email')
            ));
        }

        ///////////////////////////////////////////////////////////////////
        if ($user) {
            $email = $data['user_profile']['email'];
            if (!$this->aauth->login($email, '*****', true)) {
                $data['user_profile'] = false;
                $message = 'Seu facebook não está vinculado a nenhum usuário do Backpack.';
                echo "<script>";
                echo "alert('$message');";
                echo "location.href=''";
                echo "</script>";
            }
            $this->facebook->destroySession();
            $data['login_url'] = $this->facebook->getLoginUrl(array(
                'redirect_uri' => site_url('auth'),
                'scope' => array('email')
            ));
        }
        ///////////////////////////////////////////////////////////////////

        $this->load->view('layout/header');
        $this->load->view('layout/nav');
        $this->load->view('auth/login', $data);
        $this->load->view('layout/footer');
    }

    private function formatDate($fbdate) {
        // facebook user_birthday format (02/19/1988)
        $date = DateTime::createFromFormat('m/d/Y', $fbdate);
        return $date->format('d/m/Y');
    }

}
