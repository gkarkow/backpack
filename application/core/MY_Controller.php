<?php

class MY_Controller extends CI_Controller {

    private $msg;
    private $status;

    public function __construct() {
        parent::__construct();
        self::Zero();
    }

    private function Zero() {
        $this->msg = $this->status = null;
    }

    protected function setOK($msg = null) {
        $this->msg = $msg;
        $this->status = 'msg_ok';
        return true;
    }

    protected function setWarning($msg = null) {
        $this->msg = $msg;
        $this->status = 'msg_warning';
        return false;
    }

    protected function setError($msg = null) {
        $this->msg = $msg;
        $this->status = 'msg_error';
        return false;
    }

    public function showMessage() {
        echo "<div class=\"opac\"></div>"
        . "<div class=\"msg $this->status\">"
        . "$this->msg"
        . "</div>";
    }

    public function checkAccess($permission) {
        if (!$this->aauth->is_allowed($permission)) {
            redirect(base_url());
        }
    }
    
    public function checkManage($id) {
        if (!$this->aauth->is_admin() && strcmp($this->aauth->get_user_id(), $id) != 0) {
            redirect(base_url());
        }
    }

    public function checkSession() {
        if ($this->aauth->is_loggedin()) {
            redirect(base_url());
        }
    }

}
