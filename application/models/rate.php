<?php

class Rate extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function get_by_local($id) {
        $this->db->where('id_local', $id);
        $query = $this->db->get('wcd_rate');
        return $query->result();
    }

    public function already_voted($id_user, $id_local) {
        $this->db->where('id_user', $id_user);
        $this->db->where('id_local', $id_local);
        $rate = $this->db->get('wcd_rate');
        if ($rate->num_rows() > 0) {
            return true;
        }
        return false;
    }

    public function insert($data = array()) {
        $this->db->insert('wcd_rate', $data);
        return $this->db->affected_rows();
    }

    public function update($id_user, $id_local, $data) {
        $this->db->where('id_user', $id_user);
        $this->db->where('id_local', $id_local);
        $this->db->update('wcd_rate', $data);
        return $this->db->affected_rows();
    }

}
