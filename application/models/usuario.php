<?php

class Usuario extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function get_all() {
        $query = $this->db->get('aauth_users');
        return $query->result();
    }

    public function insert($data = array()) {
        $this->db->insert('aauth_users', $data);
        return $this->db->affected_rows();
    }

}
