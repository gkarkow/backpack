<?php

class Map extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function get_all_coordinates($input, $tipo) {
        $coords = array();
        $localidade = strtoupper($input);
        $endereco = explode(",", $localidade);

        switch ($tipo) {
            case 'hotel': $id_tipo_local = 1;
                break;
            case 'noite': $id_tipo_local = 2;
                break;
            case 'comida': $id_tipo_local = 3;
                break;
            case 'turismo': $id_tipo_local = 4;
                break;
            default : $id_tipo_local = null;
        }

        $nome_estado = "NULL";
        if (!empty($endereco[1])) {
            $nome_estado = trim($endereco[1]);
        }
        //retorna o codigo do estado
        $this->db->select('cod_estado');
        $this->db->where('nome', $nome_estado);
        $estado = $this->db->get('estado');

        //retornar cidade
        $this->db->select('cod_cidade');
        $this->db->where('nome', $endereco[0]);
        if ($estado->num_rows() > 0) {
            $est = $estado->row();
            $this->db->where('estado_cod_estado', $est->cod_estado);
        }
        $cidade = $this->db->get('cidade');

        if ($cidade->num_rows() > 0) {
            $row = $cidade->row();
            $id_cidade = $row->cod_cidade;

            $this->db->select('coordx, coordy, idlocal, nome, tipo_local_idtipo_local');
            $this->db->where('cidade_cod_cidade', $id_cidade);
            if (!empty($id_tipo_local)) {
                $this->db->where('tipo_local_idtipo_local', $id_tipo_local);
            }
            $query = $this->db->get('local');

            if ($query->num_rows() > 0) {
                foreach ($query->result() as $row) {
                    array_push($coords, $row);
                }
            }
        }
        return $coords;
    }

}
