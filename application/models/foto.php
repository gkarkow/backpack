<?php

class Foto extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function insert($opcoes = array()) {
        $this->db->insert('foto', $opcoes);
        return $this->db->affected_rows();
    }

    public function get_all_by_local($idlocal) {
        $this->db->select('foto');
        $this->db->where('local_idlocal', $idlocal);
        $query = $this->db->get('foto');
        return $query->result();
    }

}
