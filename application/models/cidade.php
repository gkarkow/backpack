<?php

class Cidade extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function get_all() {
        $query = $this->db->get('cidade');
        return $query->result();
    }
    
    public function get_by_name($nome){
        $this->db->where('nome', $nome);
        $query = $this->db->get('cidade');
        return $query->result();
    }
    
    public function get_by_id($id) {
        $this->db->where('cod_cidade', $id);
        $query = $this->db->get('cidade');
        return $query->result();
    }
    
    public function insert($opcoes = array()) {
        $this->db->insert('cidade', $opcoes);
        return $this->db->affected_rows();
    }
    
    public function update($id, $opcoes = array()){
        $this->db->where('cod_cidade', $id);
        $this->db->update('cidade', $opcoes);
        return $this->db->affected_rows();
    }
    
    public function delete($id){
        $this->db->where('cod_cidade', $id);
        $this->db->delete('cidade');
        return $this->db->affected_rows();
    }

    public function localidades($estado) {
        $this->db->where('estado_cod_estado', $estado);
        $this->db->order_by('nome', 'asc');
        $cidades = $this->db->get('cidade');
        if ($cidades->num_rows() > 0) {
            return $cidades->result();
        }
    }

}
