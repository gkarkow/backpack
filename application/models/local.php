<?php

class Local extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function get_all() {
        $query = $this->db->get('local');
        return $query->result();
    }

    public function get_all_type($tipo) {
        $this->db->where('tipo_local_idtipo_local', $tipo);
        $query = $this->db->get('local');
        return $query->result();
    }
    
    public function get_by_name($name){
        $this->db->where('nome', $name);
        $query = $this->db->get('local');
        return $query->result();
    }
    
    public function get_by_name_and_type($name, $type){
        $this->db->where('nome', $name);
        $this->db->where('tipo_local_idtipo_local', $type);
        $query = $this->db->get('local');
        return $query->result();
    }

    public function get_by_id($id) {
        $this->db->where('idlocal', $id);
        $query = $this->db->get('local');
        return $query->result();
    }

    public function insert($opcoes = array()) {
        $this->db->insert('local', $opcoes);
        return $this->db->affected_rows();
    }
    
    public function delete($id){
        $this->db->where('idlocal', $id);
        $this->db->delete('local');
        return $this->db->affected_rows();
    }
    
    public function update($id, $opcoes = array()){
        $this->db->where('idlocal', $id);
        $this->db->update('local', $opcoes);
        return $this->db->affected_rows();
    }

}
