<?php

class Tipo_Local extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function get_all() {
        $query = $this->db->get('tipo_local');
        return $query->result();
    }

    public function retorna_nome($id) {
        $this->db->select('nome');
        $this->db->where('idtipo_local', $id);
        $query = $this->db->get('tipo_local');
        if ($query->num_rows > 0) {
            $row = $query->row();
            return $a = $row->nome;
        } else {
            return $a = "NULL";
        }
    }

}
