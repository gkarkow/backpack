<?php

class Estado extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function get_all() {
        $query = $this->db->get('estado');
        return $query->result();
    }
    
    public function get_by_id($id) {
        $this->db->where('cod_estado', $id);
        $query = $this->db->get('estado');
        return $query->result();
    }
    
}
