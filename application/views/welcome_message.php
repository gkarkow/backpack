<!-- Intro Header -->
<header class="intro">
    <div class="intro-body">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <h1 class="brand-heading">backpack</h1>
                    <p class="intro-text">O destino da sua viagem</p>
                    <a href="#destino" class="btn btn-circle page-scroll">
                        <i class="fa fa-angle-double-down animated"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</header>

<section id="destino" class="content-section text-center" style="min-height: 540px">
    <div class="container">
        <div class="col-lg-8 col-lg-offset-2">
            <h3>Digite a localidade de destino</h3>
            <form class="form-horizontal" id="gmap" role="form" action="<?= base_url(); ?>main#googlemaps" method="post">
                <div class="form-group">
                    <input type="text" class="form-control" name="destino" autocomplete="off" placeholder='"Santa Maria, Rio Grande do Sul"'>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-default btn-block">Buscar</button>
                </div>
            </form>
            <script>
                $("#gmap").validate({
                    rules: {
                        destino: {
                            required: true,
                            pattern: /^([a-zA-Z-áéíóúãõâêôÁÉÍÓÚÂÊÔ]+(?:\,)?(?:(?:'| )[a-zA-Z-áéíóúãõâêôÁÉÍÓÚÂÊÔ]+(?:\,)?)*)$/
                        }
                    }
                });
            </script>
        </div>
    </div>
</section>