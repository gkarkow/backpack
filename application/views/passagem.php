<div class="container" style="min-height: 540px;">
    <header>
        <div class="text-center">
            <h1>Passagem</h1>
        </div>
    </header>

    <section>
        <div class="col-md-8 col-md-offset-2 text-center">
            <fieldset>
                <form class="form-horizontal" id="passagem" role="form" action="<?= base_url(); ?>passagem/go" method="post">
                    <div class="form-group">
                        <input type="text" class="form-control" name="origem" id="origem" autocomplete="off" placeholder="Origem">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="destino" id="destino" autocomplete="off" placeholder="Destino" value="<?= $destino; ?>">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="data" id="data" autocomplete="off" placeholder="Data de partida: 01/01/1990">
                    </div>
                    <div class="form-group" style="padding-top: 20px;">
                        <button type="submit" class="btn btn-primary btn-block">Buscar</button>
                        <a href="<?= base_url(); ?>main?destino=<?= $destino; ?>#googlemaps" class="btn btn-danger btn-block">Cancelar</a>
                    </div>
                </form>
                <script>
                    $("#passagem").validate({
                        rules: {
                            origem: {
                                required: true,
                                pattern: /^([a-zA-Z-áéíóúãõâêôÁÉÍÓÚÂÊÔ]+(?:\,)?(?:(?:'| )[a-zA-Z-áéíóúãõâêôÁÉÍÓÚÂÊÔ]+(?:\,)?)*)$/
                            },
                            destino: {
                                required: true,
                                pattern: /^([a-zA-Z-áéíóúãõâêôÁÉÍÓÚÂÊÔ]+(?:\,)?(?:(?:'| )[a-zA-Z-áéíóúãõâêôÁÉÍÓÚÂÊÔ]+(?:\,)?)*)$/
                            },
                            data: {
                                required: true,
                                dateITA: true
                            }
                        }
                    });
                </script>
            </fieldset>
        </div>
    </section>
</div>