<div class="container" style="min-height: 540px;">
    <header>
        <div class="text-center">
            <h1>Destino</h1>
        </div>
    </header>

    <section>
        <div class="col-md-8 col-md-offset-2 text-center">
            <div class="row set">
                <ul class="item">
                    <li><small>Nome</small></li>
                    <li><?php echo $nome; ?></li>
                </ul>
            </div>
            <div class="row set">
                <ul class="item">
                    <li><small>CEP</small></li>
                    <li><?php echo $cep; ?></li>
                </ul>
            </div>
            <div class="row set">
                <ul class="item">
                    <li><small>Estado</small></li>
                    <li><?php echo $estado; ?></li>
                </ul>
            </div>
            <div class="row" style="padding-top: 20px;">
                <a href="<?= base_url(); ?>main?destino=<?= $destino; ?>#googlemaps" class="btn btn-primary btn-block">Ver no mapa</a>
                <a href="<?= base_url(); ?>cidade_controller/edit?id=<?= $id; ?>" class="btn btn-info btn-block">Editar</a>
                <a href="<?= base_url(); ?>cidade_controller/delete?id=<?= $id; ?>" class="btn btn-danger btn-block" onclick="return confirm('Confirmar ação?');">Deletar</a>
            </div>
        </div>
    </section>
</div>