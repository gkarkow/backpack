<div class="container" style="min-height: 540px;">
    <header>
        <div class="text-center">
            <h1>Cidades</h1>
        </div>
    </header>
    <section>
        <div class="col-md-8 col-md-offset-2 text-center">
            <?php foreach ($cidades as $cid): ?>
                <div class="row set">
                    <a href="<?= base_url(); ?>cidade_controller/view?id=<?= $cid->cod_cidade; ?>">
                        <ul class="item">
                            <li><small>#<?= $cid->cod_cidade; ?></small></li>
                            <li><?= $cid->nome; ?></li>
                        </ul>
                    </a>
                </div>
            <?php endforeach; ?>
        </div>
    </section>
</div>