<div class="container" style="min-height: 540px;">
    <header>
        <div class="text-center">
            <h1>Busca</h1>
        </div>
    </header>

    <section>
        <div class="col-md-8 col-md-offset-2 text-center">
            <fieldset>
                <form class="form-horizontal" id="search" role="form" action="<?= base_url(); ?>cidade_controller/find" method="post">
                    <div class="form-group">
                        <input type="text" class="form-control" name="nomecidade" id="nomecidade" autocomplete="off" placeholder="Nome da cidade">
                    </div>
                    <div class="form-group" style="padding-top: 20px;">
                        <button type="submit" class="btn btn-primary btn-block">Buscar</button>
                        <a href="<?= base_url(); ?>cidade_controller" class="btn btn-danger btn-block">Cancelar</a>
                    </div>
                </form>
                <script>
                    $("#search").validate({
                        rules: {
                            nomecidade: {
                                required: true
                            }
                        }
                    });
                </script>
            </fieldset>
        </div>
    </section>
</div>