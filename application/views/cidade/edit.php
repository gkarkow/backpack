<div class="container" style="min-height: 540px;">
    <header>
        <div class="text-center">
            <h1>Editar</h1>
        </div>
    </header>
    <section>
        <div class="col-md-8 col-md-offset-2 text-center">
            <fieldset>
                <form class="form-horizontal" role="form" action="<?= base_url(); ?>cidade_controller/update" method="post">
                    <input type="hidden" class="form-control" name="id" id="id" value="<?= $cidade->cod_cidade; ?>">
                    <div class="form-group">
                        <select class="selectpicker" data-width="100%" name="estado" id="estado">
                            <?php foreach ($estados as $estado): ?>
                                <option value="<?= $estado->cod_estado; ?>"><?= $estado->sigla; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <script>
                            $('.selectpicker').selectpicker({
                                size: 5
                            });
                        </script>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="nome" id="nome" autocomplete="off" value="<?= $cidade->nome; ?>" placeholder="Nome da cidade">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="cep" id="cep" value="<?= $cidade->cep; ?>" placeholder="CEP"> 
                    </div>
                    <div class="form-group" style="padding-top: 20px;">
                        <button type="submit" class="btn btn-primary btn-block">Salvar</button>
                        <a href="<?= base_url(); ?>cidade_controller" class="btn btn-danger btn-block">Cancelar</a>
                    </div>
                </form>
            </fieldset>
        </div>
    </section>
</div>