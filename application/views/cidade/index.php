<div class="container" style="min-height: 540px">
    <header>
        <div class="text-center">
            <h1>Cidades</h1>
        </div>
    </header>

    <section>
        <div class="col-md-8 col-md-offset-2 text-center">
            <div class="row">
                <a href="<?= base_url(); ?>cidade_controller/search" class="btn btn-default btn-block" role="button">Buscar</a>
                <a href="<?= base_url(); ?>cidade_controller/state" class="btn btn-info btn-block" role="button">Listar</a>
                <a href="<?= base_url(); ?>cidade_controller/create" class="btn btn-primary btn-block" role="button">Inserir</a>
                <a href="<?= base_url(); ?>cidade_controller/search" class="btn btn-danger btn-block" role="button">Editar/Deletar</a>
            </div>
        </div>
    </section>
</div>

