<div class="container" style="min-height: 540px">
    <header>
        <div class="text-center">
            <h1>Escolha um estado</h1>
        </div>
    </header>

    <section>
        <div class="col-md-8 col-md-offset-2 text-center">
            <fieldset>
                <form class="form-horizontal" id="edit" role="form" action="<?= base_url(); ?>cidade_controller/all" method="post">
                    <div class="form-group">
                        <select class="selectpicker" data-width="100%" name="estado" id="estado">
                            <?php foreach ($estados as $estado): ?>
                                <option value="<?= $estado->cod_estado; ?>"><?= $estado->sigla; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <script>
                            $('.selectpicker').selectpicker({
                                size: 5
                            });
                        </script>
                    </div>
                    <div class="form-group" style="padding-top: 20px;">
                        <button type="submit" class="btn btn-primary btn-block">Listar</button>
                        <a href="<?= base_url(); ?>cidade_controller" class="btn btn-danger btn-block">Cancelar</a>
                    </div>
                </form>
            </fieldset>
        </div>
    </section>
</div>