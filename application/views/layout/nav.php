<!-- Navigation -->
<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand page-scroll" href="#page-top">backpack</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
            <ul class="nav navbar-nav">
                <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                <li class="hidden">
                    <a href="#page-top"></a>
                </li>
                <li>
                    <a class="page-scroll" href="<?= base_url(); ?>">Home</a>
                </li>
                <?php if ($this->aauth->is_allowed('super')): ?>
                    <li class="dropdown">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle">CRUD <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?= base_url(); ?>cidade_controller">Destino</a></li>
                            <li class="divider"></li>
                            <li><a href="<?= base_url(); ?>local_controller?tipolocal=2">Casa Noturna</a></li>
                            <li><a href="<?= base_url(); ?>local_controller?tipolocal=1">Hotel</a></li>
                            <li><a href="<?= base_url(); ?>local_controller?tipolocal=4">Ponto turístico</a></li>
                            <li><a href="<?= base_url(); ?>local_controller?tipolocal=3">Restaurante</a></li>
                            <li class="divider"></li>
                            <li><a href="<?= base_url(); ?>usuario_controller">Usuário</a></li>
                        </ul>
                    </li>
                <?php endif; ?>
                <?php if (!$this->aauth->is_loggedin()): ?>
                    <li>
                        <a class="page-scroll" href="<?= base_url(); ?>auth">Login</a>
                    </li>
                <?php else: ?>
                    <li class="dropdown">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle"><?php echo $this->aauth->get_user()->username; ?> <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?= base_url(); ?>usuario_controller/view?id=<?= $this->aauth->get_user_id(); ?>">Perfil</a></li>
                            <?php if ($this->aauth->is_allowed('super')): ?>
                                <li><a href="<?= base_url(); ?>admin">Gerenciar</a></li>
                            <?php endif; ?>
                            <li class="divider"></li>
                            <li><a class="page-scroll" href="<?= base_url(); ?>auth/logout">Logout</a></li>
                        </ul>
                    </li>
                <?php endif; ?>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>