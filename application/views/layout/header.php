<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Backpack</title>

        <link rel="shortcut icon" href="<?= base_url(); ?>backpack.ico">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>

        <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.0/jquery.validate.min.js"></script>
        <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.0/additional-methods.min.js"></script>
        <!--script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.0/localization/messages_pt_BR.js"></script-->

        <script src="<?= base_url(); ?>bootstrap/js/jquery.easing.min.js"></script>
        <script src="<?= base_url(); ?>bootstrap/js/grayscale.js"></script>
        <script src="http://markusslima.github.io/bootstrap-filestyle/js/bootstrap-filestyle.min.js"></script>

        <script src="<?= base_url(); ?>bootstrap/js/bootstrap-switch.js"></script>
        <script src="<?= base_url(); ?>bootstrap/js/bootstrap-select.js"></script>
        <script src="<?= base_url(); ?>bootstrap/js/jquery.flexslider.js"></script>
        <link href="<?= base_url(); ?>bootstrap/css/bootstrap-switch.css" rel="stylesheet" type="text/css">
        <link href="<?= base_url(); ?>bootstrap/css/bootstrap-select.css" rel="stylesheet" type="text/css">
        <link href="<?= base_url(); ?>bootstrap/css/flexslider.css" rel="stylesheet" type="text/css">

        <link href="<?= base_url(); ?>bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="<?= base_url(); ?>bootstrap/css/grayscale.css" rel="stylesheet" type="text/css">
        <link href="<?= base_url(); ?>bootstrap/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    </head>

    <body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">