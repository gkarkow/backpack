<!-- Navigation -->
<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand page-scroll" href="#page-top">backpack</a>
        </div>

        <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
            <ul class="nav navbar-nav">
                <li class="hidden">
                    <a href="#page-top"></a>
                </li>
                <li>
                    <a class="page-scroll" href="<?= base_url(); ?>">Home</a>
                </li>
                <li>
                    <a class="page-scroll" href="#destino">Destino</a>
                </li>
                <li>
                    <a class="page-scroll" href="#googlemaps">Mapa</a>
                </li>
                <li>
                    <a class="page-scroll" href="#local">Locais</a>
                </li>
                <?php if (!$this->aauth->is_loggedin()): ?>
                    <li>
                        <a class="page-scroll" href="<?= base_url(); ?>auth">Login</a>
                    </li>
                <?php else: ?>
                    <li class="dropdown">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                            <?php echo $this->aauth->get_user()->username; ?> <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="<?= base_url(); ?>usuario_controller/view?id=<?= $this->aauth->get_user_id(); ?>">Perfil</a></li>
                            <?php if ($this->aauth->is_allowed('super')): ?>
                                <li><a href="<?= base_url(); ?>admin">Gerenciar</a></li>
                            <?php endif; ?>
                            <li class="divider"></li>
                            <li><a class="page-scroll" href="<?= base_url(); ?>auth/logout">Logout</a></li>
                        </ul>
                    </li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</nav>