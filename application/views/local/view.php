<div class="container" style="min-height: 540px;">
    <header>
        <div class="text-center">
            <h1>Local</h1>
        </div>
    </header>

    <section>
        <div class="col-md-8 col-md-offset-2 text-center">

            <?php if ($photos): ?>
                <div class="row flexslider">
                    <ul class="slides">
                        <?php foreach ($photos as $img): ?>
                            <li><img src="<?= base_url(); ?>upload_fotos/<?= $img->foto; ?>"/></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <script>
                    $('.flexslider').flexslider({
                        animation: "slide"
                    });
                </script>
            <?php endif; ?>

            <div class="row set">
                <ul class="item">
                    <li><small>Nome</small></li>
                    <li><?php echo $local->nome; ?></li>
                </ul>
            </div>
            <div class="row set">
                <ul class="item">
                    <li><small>Tipo</small></li>
                    <li><?php echo $tipo_local; ?></li>
                </ul>
            </div>
            <div class="row set">
                <a href="<?= base_url(); ?>main?destino=<?= $destino; ?>#googlemaps">
                    <ul class="item">
                        <li><small>Localização</small></li>
                        <li><?php echo $destino; ?></li>
                    </ul>
                </a>
            </div>
            <div class="row set">
                <ul class="item">
                    <li><small>Avaliação</small></li>
                    <li>Avaliado <strong><?= $rate_times; ?></strong> vezes / Média <strong><?= $rate_value; ?></strong> de 5</li>
                    <div class="rate-result-cnt" style="margin: 4px auto;">
                        <div class="rate-bg" style="width:<?= $rate_bg; ?>%"></div>
                        <div class="rate-stars"></div>
                    </div>
                </ul>
            </div>

            <?php if ($this->aauth->is_allowed('normal')): ?>
                <div class="row set" style="background-color: #f6f6f6;">
                    <div class="rate-ex2-cnt" style="margin: 0 auto;">
                        <div id="1" class="rate-btn-1 rate-btn"></div>
                        <div id="2" class="rate-btn-2 rate-btn"></div>
                        <div id="3" class="rate-btn-3 rate-btn"></div>
                        <div id="4" class="rate-btn-4 rate-btn"></div>
                        <div id="5" class="rate-btn-5 rate-btn"></div>
                    </div>
                    <script>
                        $(function () {
                            $('.rate-btn').hover(function () {
                                $('.rate-btn').removeClass('rate-btn-hover');
                                var therate = $(this).attr('id');
                                for (var i = therate; i >= 0; i--) {
                                    $('.rate-btn-' + i).addClass('rate-btn-hover');
                                }
                                ;
                            });

                            $('.rate-btn').click(function () {
                                var idlocal = <?= $id; ?>;
                                var rate = $(this).attr('id');
                                var msg = "Avaliação computada!";
                                $('.rate-btn').removeClass('rate-btn-active');
                                for (var i = rate; i >= 0; i--) {
                                    $('.rate-btn-' + i).addClass('rate-btn-active');
                                }
                                ;
                                $.ajax({
                                    type: 'POST',
                                    url: '<?php echo site_url("local_controller/rating") ?>',
                                    data: {idlocal: idlocal, rate: rate},
                                    success: function () {
                                        alert(msg);
                                    }
                                });
                            });
                        });
                    </script>
                </div>
            <?php endif; ?>

            <div class="row" style="padding-top: 20px;">
                <?php if ($this->aauth->is_allowed('normal')): ?>
                    <a href="<?= base_url(); ?>foto_controller?idlocal=<?= $id; ?>" class="btn btn-primary btn-block">Upload de foto</a>
                <?php endif; ?>
                <?php if ($this->aauth->is_allowed('super')): ?>
                    <a href="<?= base_url(); ?>local_controller/edit?id=<?= $id; ?>" class="btn btn-info btn-block">Editar</a>
                    <a href="<?= base_url(); ?>local_controller/delete?id=<?= $id; ?>&type=<?= $type; ?>" class="btn btn-danger btn-block" onclick="return confirm('Confirmar ação?');">Deletar</a>
                <?php endif; ?>
            </div>

            <?php if ($this->aauth->is_allowed('normal')): ?>
                <div class="row" id="disqus_thread" style="margin-top: 40px;"></div>
                <script type="text/javascript">
                    var disqus_config = function () {
                        this.page.remote_auth_s3 = '<?php echo "$message $hmac $timestamp"; ?>';
                        this.page.api_key = '<?php echo $publickey; ?>';
                    };
                </script>
            <?php else: ?>
                <div class="row" id="disqus_thread" style="margin-top: 40px; pointer-events:none;"></div>
            <?php endif; ?>
            <script type="text/javascript">
                /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
                var disqus_shortname = 'backpackps'; // required: replace example with your forum shortname
                var disqus_identifier = '<?= $id; ?>';

                /* * * DON'T EDIT BELOW THIS LINE * * */
                (function () {
                    var dsq = document.createElement('script');
                    dsq.type = 'text/javascript';
                    dsq.async = true;
                    dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
                })();
            </script>
            <noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
        </div>
    </section>
</div>