<div class="container" style="min-height: 540px;">
    <header>
        <div class="text-center">
            <h1>Cadastro</h1>
        </div>
    </header>

    <section>
        <div class="text-center">
            <div id="mapa" style="height: 400px; margin: 0 auto 20px auto;"></div>
        </div>
        <script>
            var lat = '-13.841393';
            var lng = '-49.741256';
            function initialize() {
                var mapOptions = {
                    zoom: 4,
                    center: new google.maps.LatLng(lat, lng),
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                var map = new google.maps.Map(document.getElementById('mapa'), mapOptions);

                var myMarker = new google.maps.Marker({
                    position: new google.maps.LatLng(-13.841393, -49.741256),
                    //animation: google.maps.Animation.DROP, 
                    draggable: true
                });

                map.setCenter(myMarker.position);

                myMarker.setMap(map);

                google.maps.event.addListener(myMarker, 'dragend', function (evt) {
                    document.getElementById('coordx').value = myMarker.getPosition().lat();
                    document.getElementById('coordy').value = myMarker.getPosition().lng();
                });
            }
            google.maps.event.addDomListener(window, 'load', initialize);
        </script>
    </section>

    <section>
        <div class="col-md-8 col-md-offset-2 text-center">
            <fieldset>
                <form class="form-horizontal" role="form" action="<?= base_url(); ?>local_controller/insert" method="post">
                    <input type="hidden" name="idtipo_local" id="idtipo_local" value="<?= $tipolocal; ?>">
                    <div class="form-group">
                        <input type="text" class="form-control" name="nome" id="nome" autocomplete="off" placeholder="Nome">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="coordx" id="coordx" autocomplete="off" placeholder="Coordenada X -- Arraste o marcador no mapa para adicionar a coordenada">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="coordy" id="coordy" autocomplete="off" placeholder="Coordenada Y -- Arraste o marcador no mapa para adicionar a coordenada">
                    </div>
                    <div class="form-group">
                        <select class="selectpicker" data-width="100%" name="estado" id="estado">
                            <?php foreach ($estados as $estado): ?>
                                <option value="<?= $estado->cod_estado; ?>"><?= $estado->sigla; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <script>
                            $('.selectpicker').selectpicker({
                                size: 5
                            });
                        </script>
                    </div>
                    <div class="form-group" id="city"></div>
                    <script type="text/javascript">
                        $(document).ready(function () {
                            $("#estado").change(function () {
                                $("#estado option:selected").each(function () {
                                    estado = $('#estado').val();
                                    $.post("http://localhost/backpack/local_controller/chama_cidades", {
                                        estado: estado
                                    }, function (data) {
                                        $("#city").html(data);
                                    });
                                });
                            });
                        });
                    </script>
                    <div class="form-group" style="padding-top: 20px;">
                        <button type="submit" class="btn btn-primary btn-block">Salvar</button>
                        <a href="<?= base_url(); ?>local_controller?tipolocal=<?= $tipolocal; ?>" class="btn btn-danger btn-block">Cancelar</a>
                    </div>
                </form>
            </fieldset>
        </div>
    </section>
</div>
