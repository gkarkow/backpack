<div class="container" style="min-height: 540px;">
    <header>
        <div class="text-center">
            <h1>Busca</h1>
        </div>
    </header>

    <section>
        <div class="col-md-8 col-md-offset-2 text-center">
            <fieldset>
                <form class="form-horizontal" id="search" role="form" action="<?= base_url(); ?>local_controller/find" method="post">
                    <input type="text" name="tipolocal" hidden="true" value="<?= $tipolocal; ?>">
                    <div class="form-group">
                        <input type="text" class="form-control" name="nomelocal" id="nomelocal" autocomplete="off" placeholder="Nome do local">
                    </div>
                    <div class="form-group" style="padding-top: 20px;">
                        <button type="submit" class="btn btn-primary btn-block">Buscar</button>
                        <a href="<?= base_url(); ?>local_controller?tipolocal=<?= $tipolocal; ?>" class="btn btn-danger btn-block">Cancelar</a>
                    </div>
                </form>
                <script>
                    $("#search").validate({
                        rules: {
                            nomelocal: {
                                required: true
                            }
                        }
                    });
                </script>
            </fieldset>
        </div>
    </section>
</div>