<div class="container" style="min-height: 540px;">
    <header>
        <div class="text-center">
            <h1>Editar</h1>
        </div>
    </header>
    <section>
        <div class="text-center">
            <div id="mapa" style="height: 400px; margin: 0 auto 20px auto;"></div>
        </div>
        <script>
            var lat = '-13.841393';
            var lng = '-49.741256';
            function initialize() {
                var mapOptions = {
                    zoom: 13,
                    center: new google.maps.LatLng(lat, lng),
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                var map = new google.maps.Map(document.getElementById('mapa'), mapOptions);

                var myMarker = new google.maps.Marker({
                    position: new google.maps.LatLng(<?php echo $local->coordx; ?>, <?php echo $local->coordy; ?>),
                    //animation: google.maps.Animation.DROP, 
                    draggable: true
                });

                map.setCenter(myMarker.position);

                myMarker.setMap(map);

                google.maps.event.addListener(myMarker, 'dragend', function (evt) {
                    document.getElementById('coordx').value = myMarker.getPosition().lat();
                    document.getElementById('coordy').value = myMarker.getPosition().lng();
                });
            }
            google.maps.event.addDomListener(window, 'load', initialize);
        </script>
    </section>
    <section>
        <div class="col-md-8 col-md-offset-2 text-center">
            <fieldset>
                <form class="form-horizontal" id="edit" role="form" action="<?= base_url(); ?>local_controller/update" method="post">
                    <input type="text" name="idlocal" hidden="true" value="<?= $local->idlocal; ?>">
                    <input type="text" name="tipo" hidden="true" value="<?= $local->tipo_local_idtipo_local; ?>">
                    <div class="form-group">
                        <input type="text" class="form-control" name="nome" id="nome" autocomplete="off" value="<?= $local->nome ?>" placeholder="Nome do local">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="coordx" id="coordx" value="<?= $local->coordx; ?>" autocomplete="off" placeholder="Coordenada X -- Arraste o marcador no mapa para adicionar a coordenada">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="coordy" id="coordy" value="<?= $local->coordy; ?>"autocomplete="off" placeholder="Coordenada Y -- Arraste o marcador no mapa para adicionar a coordenada">
                    </div>
                    <div class="form-group">
                        <select class="selectpicker" data-width="100%" name="estado" id="estado">
                            <?php foreach ($estados as $estado): ?>
                                <?php if ($estado->sigla == $e->sigla): ?>
                                    <option value="<?= $estado->cod_estado; ?>" selected="true"><?= $estado->sigla; ?></option>
                                <?php else: ?>
                                    <option value="<?= $estado->cod_estado; ?>"><?= $estado->sigla; ?></option>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </select>
                        <script>
                            $('.selectpicker').selectpicker({
                                size: 5
                            });
                        </script>
                    </div>
                    <div class="form-group" id="city">
                        <select class="selectpicker" data-width="100%" name="cidade">
                            <option value="<?= $c->cod_cidade; ?>" selected="true"><?= $c->nome; ?></option>
                        </select>
                        <script>
                            $('.selectpicker').selectpicker({
                                size: 5
                            });
                        </script>
                    </div>
                    <script type="text/javascript">
                        $(document).ready(function () {
                            $("#estado").change(function () {
                                $("#estado option:selected").each(function () {
                                    estado = $('#estado').val();
                                    $.post("http://localhost/backpack/local_controller/chama_cidades", {
                                        estado: estado
                                    }, function (data) {
                                        $("#city").html(data);
                                    });
                                });
                            });
                        });
                    </script>
                    <div class="form-group" style="padding-top: 20px;">
                        <button type="submit" class="btn btn-primary btn-block">Salvar</button>
                        <a href="<?= base_url(); ?>local_controller/view?id=<?= $local->idlocal; ?>" class="btn btn-danger btn-block">Cancelar</a>
                    </div>
                </form>
            </fieldset>
        </div>
    </section>
</div>