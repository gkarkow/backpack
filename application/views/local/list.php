<div class="container" style="min-height: 540px;">
    <header>
        <div class="text-center">
            <h1>Locais</h1>
        </div>
    </header>
    <section>
        <div class="col-md-8 col-md-offset-2 text-center">
            <?php foreach ($local as $locais): ?>
                <div class="row set">
                    <a href="<?= base_url(); ?>local_controller/view?id=<?= $locais->idlocal; ?>">
                        <ul class="item">
                            <li><small>#<?= $locais->idlocal; ?></small></li>
                            <li><?= $locais->nome; ?></li>
                        </ul>
                    </a>
                </div>
            <?php endforeach; ?>
        </div>
    </section>
</div>

