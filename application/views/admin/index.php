<div class="container" style="min-height: 540px">
    <header>
        <div class="text-center">
            <h1>Admin</h1>
        </div>
    </header>

    <section>
        <div class="col-md-8 col-md-offset-2 text-center">
            <div class="row">
                <h3><small>Restricted area</small></h3>
            </div>
            <div class="row set">
                <ul class="item">
                    <li><small>Administradores</small></li>
                    <li><?= $admin; ?></li>
                </ul>
            </div>
            <div class="row set">
                <ul class="item">
                    <li><small>Usuários</small></li>
                    <li><?= $users; ?></li>
                </ul>
            </div>
        </div>
    </section>
</div>