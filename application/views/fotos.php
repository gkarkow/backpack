<div class="container" style="min-height: 540px;">
    <header>
        <div class="text-center">
            <h1>Foto</h1>
        </div>
    </header>

    <div class="col-md-8 col-md-offset-2 text-center">
        <fieldset>
            <form id="foto" name="insertfoto" action="<?= base_url(); ?>foto_controller/add_foto" enctype="multipart/form-data" method="post">
                <input type="text" name="idlocal" hidden="true" value="<?= $idlocal; ?>">
                <div class="form-group">
                    <input type="file" id="userfile" name="userfile" class="filestyle" data-buttonName="btn-info" data-iconName="glyphicon-inbox" data-buttonText="Browse"/>
                </div>
                <div class="form-group" style="padding-top: 20px;">
                    <button type="submit" class="btn btn-primary btn-block">Upload</button>
                    <a href="<?= base_url(); ?>local_controller/view?id=<?= $idlocal; ?>" class="btn btn-danger btn-block">Cancelar</a>
                </div>
            </form>
            <script>
                $("#foto").validate({
                    rules: {
                        userfile: {
                            required: true,
                            extension: "gif|jpg|png"
                        }
                    }
                });
            </script>
        </fieldset>
    </div>
</div>