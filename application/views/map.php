<div class="container">
    <header style="padding-bottom: 0;">
        <div class="text-center">
            <h1>Destino</h1>
        </div>
    </header>

    <section id="destino" style="height: 120px;">
        <div class="col-md-8 col-md-offset-2 text-center s-padding">
            <form role="form" class="form-horizontal" id="gmaps" action="<?= base_url(); ?>main#googlemaps" method="post">
                <div class="form-group">
                    <input type="text" class="form-control" name="destino" autocomplete="off" placeholder="Digite a localidade de destino">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-default btn-block">Buscar</button>
                </div>
            </form>
            <script>
                $("#gmaps").validate({
                    rules: {
                        destino: {
                            required: true,
                            pattern: /^([a-zA-Z-áéíóúãõâêôÁÉÍÓÚÂÊÔ]+(?:\,)?(?:(?:'| )[a-zA-Z-áéíóúãõâêôÁÉÍÓÚÂÊÔ]+(?:\,)?)*)$/
                        }
                    }
                });
            </script>
        </div>
    </section>

    <section id="googlemaps">
        <div class="text-center s-padding">
            <div id="map-canvas"></div>
            <div class="form-group" style="padding: 40px 0 0 0;">
                <div class="row" style="display: inline-block; width: 200px;">
                    <h2 class="h4"><small>Casa noturna</small></h2>
                    <input type="checkbox" name="noite" data-off-color="danger">
                </div>
                <div class="row" style="display: inline-block; width: 200px;">
                    <h2 class="h4"><small>Hotel</small></h2>
                    <input type="checkbox" name="hotel" data-off-color="danger">
                </div>
                <div class="row" style="display: inline-block; width: 200px;">
                    <h2 class="h4"><small>Ponto turístico</small></h2>
                    <input type="checkbox" name="turismo" data-off-color="danger">
                </div>
                <div class="row" style="display: inline-block; width: 200px;">
                    <h2 class="h4"><small>Restaurante</small></h2>
                    <input type="checkbox" name="comida" data-off-color="danger">
                </div>
                <script>
                    $("[name='noite']").bootstrapSwitch('state', false, true);
                    $("[name='hotel']").bootstrapSwitch('state', false, true);
                    $("[name='turismo']").bootstrapSwitch('state', false, true);
                    $("[name='comida']").bootstrapSwitch('state', false, true);
                </script>
                <div class="row" style="display: inline-block; width: 200px;">
                    <h2 class="h4"><small>Passagem</small></h2>
                    <a href="<?= base_url(); ?>passagem?destino=<?= $destino; ?>" class="btn btn-info btn-sm" role="button" style="width: 100px;">Buscar</a>
                </div>
            </div>
            <script>
                var markersmap = [];
                var markers = [];
                var lat = <?= $lat; ?>;
                var lng = <?= $lng; ?>;
                var markers = <?php echo json_encode($markers) ?>;
                var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';
                var ic;

                function initialize() {
                    var mapOptions = {
                        zoom: 13,
                        center: new google.maps.LatLng(lat, lng),
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    };
                    var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

                    $.each(markers, function (index, m) {
                        switch (m.type) {
                            case 'noite':
                                ic = iconBase + 'bars_maps.png';
                                break;
                            case 'hotel':
                                ic = iconBase + 'lodging_maps.png';
                                break;
                            case 'turismo':
                                ic = iconBase + 'museum_maps.png';
                                break;
                            case 'comida':
                                ic = iconBase + 'rec_dining_maps.png';
                                break;
                        }
                        markersmap[index] = new google.maps.Marker({
                            position: new google.maps.LatLng(m.long, m.lat),
                            icon: ic,
                            map: map
                        });
                        markersmap[index].info = new google.maps.InfoWindow({
                            content: '<div id="content" style="min-width: 300px; color: #000;">' +
                                    '<h3><small>' + m.name + '</small></h3>' +
                                    '<p><a href="http://localhost/backpack/local_controller/view?id=' + m.id + '">' +
                                    'Mais informações</a>' +
                                    '</p>' +
                                    '</div>'
                        });
                        google.maps.event.addListener(markersmap[index], 'click', function () {
                            markersmap[index].info.open(map, markersmap[index]);
                        });
                    });

                }
                google.maps.event.addDomListener(window, 'load', initialize);

                //$("input:checkbox").bind("change", function ()
                $('input:checkbox').on('switchChange.bootstrapSwitch', function ()
                {
                    if (!$("input:checkbox[name='noite']").is(':checked') &&
                            !$("input:checkbox[name='hotel']").is(':checked') &&
                            !$("input:checkbox[name='turismo']").is(':checked') &&
                            !$("input:checkbox[name='comida']").is(':checked'))
                    {
                        $.each(markers, function (index)
                        {
                            markersmap[index].setVisible(true);
                        });
                    }
                    else
                    {
                        $.each(markers, function (index, m)
                        {
                            if ($("input:checkbox[name='" + m.type + "']").is(':checked'))
                            {
                                markersmap[index].setVisible(true);
                            }
                            else
                            {
                                markersmap[index].setVisible(false);
                            }
                        });
                    }
                });

            </script>
        </div>
    </section>

    <section id="local">
        <div class="col-md-8 col-md-offset-2 text-center s-padding">
            <h2>Locais sugeridos</h2>
            <h3><small>Ranking</small></h3>
            <?php foreach ($locais as $l): ?>
                <div class="row set">
                    <a href="<?= base_url(); ?>local_controller/view?id=<?= $l['id']; ?>">
                        <ul class="item">
                            <li><small><?= $l['tipo']; ?></small></li>
                            <li><?= $l['nome']; ?></li>
                            <div class="rate-result-cnt" style="margin: 4px auto;">
                                <div class="rate-bg" style="width:<?= $l['nota']; ?>%"></div>
                                <div class="rate-stars"></div>
                            </div>
                        </ul>
                    </a>
                </div>
            <?php endforeach; ?>
            <?php if (!$locais) : ?>
                <div class="row set">
                    <ul class="item">
                        <li>Nada encontrado</li>
                    </ul>
                </div>
            <?php endif; ?>
        </div>
    </section>
</div>