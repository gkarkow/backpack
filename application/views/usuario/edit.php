<div class="container" style="min-height: 540px;">
    <header>
        <div class="text-center">
            <h1>Editar</h1>
        </div>
    </header>

    <section>
        <div class="col-md-8 col-md-offset-2 text-center">
            <fieldset>
                <form class="form-horizontal" id="edit" role="form" action="<?= base_url(); ?>usuario_controller/update" method="post">
                    <input type="text" name="id" hidden="true" value="<?= $id; ?>">
                    <div class="form-group">
                        <input type="text" class="form-control" name="nome" id="nome" autocomplete="off" placeholder="Nome" value="<?= $user->name; ?>">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="mail" id="mail" autocomplete="off" placeholder="Email" value="<?= $user->email; ?>">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="username" id="username" autocomplete="off" placeholder="Nome de usuário" value="<?= $user->username; ?>">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="sexo" id="sexo" autocomplete="off" placeholder="Sexo: m/f" value="<?= $user->gender; ?>">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="dtnasc" id="dtnasc" autocomplete="off" placeholder="Data de nascimento: 01/01/1990" value="<?= $birthday; ?>">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="fone" id="fone" autocomplete="off" placeholder="Telefone: (99) 9999-9999" value="<?= $user->tel; ?>">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="endereco" id="endereco" autocomplete="off" placeholder="Endereço" value="<?= $user->location; ?>">
                    </div>
                    <div class="form-group">
                        <?php if ($this->aauth->is_allowed('super')): ?>
                            <div class="row" style="display: inline-block; margin-right: 30px;">
                                <h2 class="h4"><small>Admin</small></h2>
                                <input type="checkbox" name="admin" data-off-color="danger">
                            </div>
                            <?php if ($admin): ?>
                                <script>
                                    $("[name='admin']").bootstrapSwitch('state', true, true);
                                </script>
                            <?php else: ?>
                                <script>
                                    $("[name='admin']").bootstrapSwitch();
                                </script>
                            <?php endif; ?>
                        <?php endif; ?>
                        <div class="row" style="display: inline-block;">
                            <h2 class="h4"><small>Facebook</small></h2>
                            <input type="checkbox" name="fb_access" data-off-color="danger">
                        </div>
                        <?php if ($fb): ?>
                            <script>
                                $("[name='fb_access']").bootstrapSwitch('state', true, true);
                            </script>
                        <?php else: ?>
                            <script>
                                $("[name='fb_access']").bootstrapSwitch();
                            </script>
                        <?php endif; ?>
                    </div>
                    <div class="form-group" style="padding-top: 20px;">
                        <button type="submit" class="btn btn-primary btn-block">Salvar</button>
                        <a href="<?= base_url(); ?>usuario_controller/view?id=<?= $id; ?>" class="btn btn-danger btn-block">Cancelar</a>
                    </div>
                </form>
                <script>
                    $("#edit").validate({
                        rules: {
                            nome: {
                                required: true,
                                pattern: /^([a-zA-Z-áéíóúãõâêô]+(?:\.)?(?:(?:'| )[a-zA-Z-áéíóúãõâêô]+(?:\.)?)*)$/
                            },
                            mail: {
                                required: true,
                                email: true
                            },
                            username: {
                                required: true,
                                alphanumeric: true
                            },
                            sexo: {
                                pattern: /^(?:m|M|f|F)$/
                            },
                            dtnasc: {
                                dateITA: true
                            },
                            fone: {
                                pattern: /^\(\d{2}\) \d{4}\-\d{4}?$/
                            }
                        }
                    });
                </script>
            </fieldset>
        </div>
    </section>
</div>