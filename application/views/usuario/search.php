<div class="container" style="min-height: 540px;">
    <header>
        <div class="text-center">
            <h1>Busca</h1>
        </div>
    </header>

    <section>
        <div class="col-md-8 col-md-offset-2 text-center">
            <fieldset>
                <form class="form-horizontal" id="search" role="form" action="<?= base_url(); ?>usuario_controller/find" method="post">
                    <div class="form-group">
                        <input type="text" class="form-control" name="mail" id="mail" autocomplete="off" placeholder="Email">
                    </div>
                    <div class="form-group" style="padding-top: 20px;">
                        <button type="submit" class="btn btn-primary btn-block">Buscar</button>
                        <a href="<?= base_url(); ?>usuario_controller" class="btn btn-danger btn-block">Cancelar</a>
                    </div>
                </form>
                <script>
                    $("#search").validate({
                        rules: {
                            mail: {
                                required: true
                            }
                        }
                    });
                </script>
            </fieldset>
        </div>
    </section>
</div>