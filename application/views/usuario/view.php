<div class="container" style="min-height: 540px;">
    <header>
        <div class="text-center">
            <h1>Perfil</h1>
        </div>
    </header>

    <section>
        <div class="col-md-8 col-md-offset-2 text-center">
            <div class="row set">
                <ul class="item">
                    <li><small>Nome</small></li>
                    <li><?php echo $user->name; ?></li>
                </ul>
            </div>
            <div class="row set">
                <ul class="item">
                    <li><small>Email</small></li>
                    <li><?php echo $user->email; ?></li>
                </ul>
            </div>
            <div class="row set">
                <ul class="item">
                    <li><small>Nome de usuário</small></li>
                    <li><?php echo $user->username; ?></li>
                </ul>
            </div>
            <div class="row set">
                <ul class="item">
                    <li><small>Sexo</small></li>
                    <?php if (!empty($user->gender)): ?>
                        <li><?php echo $user->gender; ?></li>
                    <?php else: ?>
                        <li>-</li>
                    <?php endif; ?>
                </ul>
            </div>
            <div class="row set">
                <ul class="item">
                    <li><small>Data de nascimento</small></li>
                    <?php if (!empty($birthday)): ?>
                        <li><?php echo $birthday; ?></li>
                    <?php else: ?>
                        <li>-</li>
                    <?php endif; ?>
                </ul>
            </div>
            <div class="row set">
                <ul class="item">
                    <li><small>Telefone</small></li>
                    <?php if (!empty($user->tel)): ?>
                        <li><?php echo $user->tel; ?></li>
                    <?php else: ?>
                        <li>-</li>
                    <?php endif; ?>
                </ul>
            </div>
            <div class="row set">
                <ul class="item">
                    <li><small>Endereço</small></li>
                    <?php if (!empty($user->location)): ?>
                        <li><?php echo $user->location; ?></li>
                    <?php else: ?>
                        <li>-</li>
                    <?php endif; ?>
                </ul>
            </div>
            <div class="row set">
                <ul class="item">
                    <li><small>Redes sociais</small></li>
                    <?php if ($user->fb_access): ?>
                        <li>Facebook</li>
                    <?php else: ?>
                        <li>-</li>
                    <?php endif; ?>
                </ul>
            </div>
            <?php if ($this->aauth->is_allowed('super')): ?>
            <div class="row set">
                <ul class="item">
                    <li><small>Administrador</small></li>
                    <?php if ($admin): ?>
                        <li>Sim</li>
                    <?php else: ?>
                        <li>Não</li>
                    <?php endif; ?>
                </ul>
            </div>
            <?php endif; ?>
            <div class="row" style="padding-top: 20px;">
                <a href="<?= base_url(); ?>usuario_controller/password?id=<?= $id; ?>" class="btn btn-primary btn-block">Trocar senha</a>
                <a href="<?= base_url(); ?>usuario_controller/edit?id=<?= $id; ?>" class="btn btn-info btn-block">Editar</a>
                <a href="<?= base_url(); ?>usuario_controller/delete?id=<?= $id; ?>" class="btn btn-danger btn-block" onclick="return confirm('Confirmar ação?');">Deletar</a>
            </div>
        </div>
    </section>
</div>