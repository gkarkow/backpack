<div class="container" style="min-height: 540px;">
    <header>
        <div class="text-center">
            <h1>Trocar senha</h1>
        </div>
    </header>

    <section>
        <div class="col-md-8 col-md-offset-2 text-center">
            <fieldset>
                <form class="form-horizontal" id="changepass" role="form" action="<?= base_url(); ?>usuario_controller/changePassword" method="post">
                    <input type="text" name="id" value="<?= $id; ?>" hidden="true">
                    <div class="form-group">
                        <input type="password" class="form-control" name="pass1" id="pass1" autocomplete="off" placeholder="Nova senha">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="pass2" id="pass2" autocomplete="off" placeholder="Confirme a nova senha">
                    </div>
                    <div class="form-group" style="padding-top: 20px;">
                        <button type="submit" class="btn btn-primary btn-block">Salvar</button>
                        <a href="<?= base_url(); ?>usuario_controller/view?id=<?= $id; ?>" class="btn btn-danger btn-block">Cancelar</a>
                    </div>
                </form>
                <script>
                    $("#changepass").validate({
                        rules: {
                            pass1: {
                                required: true,
                                alphanumeric: true,
                                minlength: 5,
                                maxlength: 13
                            },
                            pass2: {
                                alphanumeric: true,
                                minlength: 5,
                                maxlength: 13,
                                equalTo: "#pass1"
                            }
                        }
                    });
                </script>
            </fieldset>
        </div>
    </section>
</div>