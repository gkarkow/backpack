<div class="container" style="min-height: 540px;">
    <header>
        <div class="text-center">
            <h1>Usuários</h1>
        </div>
    </header>

    <section>
        <div class="col-md-8 col-md-offset-2 text-center">
            <?php foreach ($users as $usr): ?>
                <div class="row set">
                    <a href="<?= base_url(); ?>usuario_controller/view?id=<?= $usr->id; ?>">
                        <ul class="item">
                            <li><small>#<?= $usr->id; ?></small></li>
                            <li><?= $usr->email; ?></li>
                        </ul>
                    </a>
                </div>
            <?php endforeach; ?>
        </div>
    </section>
</div>