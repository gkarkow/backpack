<div class="container" style="min-height: 540px;">
    <header>
        <div class="text-center">
            <h1>Cadastro</h1>
        </div>
    </header>

    <section>
        <div class="col-md-8 col-md-offset-2 text-center">
            <fieldset>
                <form class="form-horizontal" id="signup" role="form" action="<?= base_url(); ?>usuario_controller/insert" method="post">
                    <?php if ($user_profile): ?>
                        <div class="form-group">
                            <input type="text" class="form-control" name="nome" id="nome" autocomplete="off" placeholder="Nome" value="<?= $user_profile['name']; ?>">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="mail" id="mail" autocomplete="off" placeholder="Email" value="<?= $user_profile['email']; ?>">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" name="pass" id="pass" placeholder="Senha">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="username" id="username" autocomplete="off" placeholder="Nome de usuário">
                        </div>
                        <div class="form-group">
                            <?php if ($user_profile['gender'] == 'male'): ?>
                                <input type="text" class="form-control" name="sexo" id="sexo" autocomplete="off" placeholder="Sexo: m/f" value="m">
                            <?php else: ?>
                                <input type="text" class="form-control" name="sexo" id="sexo" autocomplete="off" placeholder="Sexo: m/f" value="f">
                            <?php endif; ?>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="dtnasc" id="dtnasc" autocomplete="off" placeholder="Data de nascimento: 01/01/1990" value="<?= $user_profile['birthday']; ?>">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="fone" id="fone" autocomplete="off" placeholder="Telefone: (99) 9999-9999">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="endereco" id="endereco" autocomplete="off" placeholder="Endereço" value="<?= $user_profile['location']['name']; ?>">
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <h2 class="h4"><small>Facebook</small></h2>
                                <input type="checkbox" name="fb" data-off-color="danger">
                            </div>
                        </div>
                        <script>
                            $("[name='fb']").bootstrapSwitch('state', true, true);
                        </script>
                    <?php else: ?>
                        <?php if (!$this->aauth->is_allowed('super')): ?>
                            <div class="form-group">
                                <a href="<?= $login_url; ?>" class="btn btn-default btn-block" role="button">Importar dados do Facebook</a>
                            </div>
                        <?php endif; ?>
                        <div class="form-group">
                            <input type="text" class="form-control" name="nome" id="nome" autocomplete="off" placeholder="Nome">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="mail" id="mail" autocomplete="off" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" name="pass" id="pass" placeholder="Senha">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="username" id="username" autocomplete="off" placeholder="Nome de usuário">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="sexo" id="sexo" autocomplete="off" placeholder="Sexo: m/f">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="dtnasc" id="dtnasc" autocomplete="off" placeholder="Data de nascimento: 01/01/1990">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="fone" id="fone" autocomplete="off" placeholder="Telefone: (99) 9999-9999">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="endereco" id="endereco" autocomplete="off" placeholder="Endereço">
                        </div>
                        <div class="form-group">
                            <?php if ($this->aauth->is_allowed('super')): ?>
                                <div class="row" style="display: inline-block; margin-right: 30px;">
                                    <h2 class="h4"><small>Admin</small></h2>
                                    <input type="checkbox" name="admin" data-off-color="danger">
                                </div>
                                <script>
                                    $("[name='admin']").bootstrapSwitch();
                                </script>
                            <?php endif; ?>
                            <div class="row" style="display: inline-block;">
                                <h2 class="h4"><small>Facebook</small></h2>
                                <input type="checkbox" name="fb" data-off-color="danger" readonly="true">
                            </div>
                            <script>
                                $("[name='fb']").bootstrapSwitch();
                            </script>
                        </div>
                    <?php endif; ?>
                    <div class="form-group" style="padding-top: 20px;">
                        <button type="submit" class="btn btn-primary btn-block">Cadastrar</button>
                        <a href="<?= base_url(); ?>usuario_controller" class="btn btn-danger btn-block">Cancelar</a>
                    </div>
                </form>
                <script>
                    $("#signup").validate({
                        rules: {
                            nome: {
                                required: true,
                                pattern: /^([a-zA-Z-áéíóúãõâêôÁÉÍÓÚÂÊÔ]+(?:\.)?(?:(?:'| )[a-zA-Z-áéíóúãõâêôÁÉÍÓÚÂÊÔ]+(?:\.)?)*)$/
                            },
                            mail: {
                                required: true,
                                email: true
                            },
                            pass: {
                                required: true,
                                alphanumeric: true,
                                minlength: 5,
                                maxlength: 13
                            },
                            username: {
                                required: true,
                                alphanumeric: true
                            },
                            sexo: {
                                pattern: /^(?:m|M|f|F)$/
                            },
                            dtnasc: {
                                dateITA: true
                            },
                            fone: {
                                pattern: /^\(\d{2}\) \d{4}\-\d{4}?$/
                            }
                        }
                    });
                </script>
            </fieldset>
        </div>
    </section>
</div>