<div class="container" style="min-height: 540px;">
    <header>
        <div class="text-center">
            <h1>Login</h1>
        </div>
    </header>

    <section>
        <div class="col-md-8 col-md-offset-2 text-center">
            <fieldset>
                <form class="form-horizontal" id="signin" role="form" action="<?= base_url(); ?>auth/login" method="post">
                    <div class="form-group">
                        <input type="text" class="form-control" name="mail" id="mail" autocomplete="off" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="pass" id="pass" placeholder="Senha">
                    </div>
                    <div class="form-group" style="padding-top: 20px;">
                        <button type="submit" class="btn btn-primary btn-block">Entrar</button>
                        <a href="<?= base_url(); ?>auth" class="btn btn-danger btn-block">Cancelar</a>
                    </div>
                </form>
                <script>
                    $("#signin").validate({
                        rules: {
                            mail: {
                                required: true,
                                email: true
                            },
                            pass: {
                                required: true,
                                alphanumeric: true,
                                minlength: 5,
                                maxlength: 13
                            }
                        }
                    });
                </script>
            </fieldset>
        </div>
    </section>
</div>