<div class="container" style="min-height: 540px;">
    <header>
        <div class="text-center">
            <h1>Login</h1>
        </div>
    </header>

    <section>
        <div class="col-md-8 col-md-offset-2 text-center">
            <form class="form-horizontal" role="form">
                <?php if ($user_profile): ?>
                    <div class="form-group">
                        <img class="img-thumbnail img-circle" data-src="holder.js/140x140" alt="140x140" src="https://graph.facebook.com/<?= $user_profile['id'] ?>/picture?type=large" style="width: 140px; height: 140px;">
                        <h3 style="margin-top: 20px;"><?= $user_profile['name']; ?></h3>
                        <a href="<?= $user_profile['link']; ?>" class="btn btn-default btn-block" role="button" target="_blank">Timeline</a>
                        <a href="<?= base_url(); ?>auth/logout" class="btn btn-primary btn-block" role="button">Logout</a>
                    </div>
                <?php else: ?>
                    <div class="form-group">
                        <a href="<?= base_url(); ?>auth/signin" class="btn btn-primary btn-block" role="button">Email/senha</a>
                        <a href="<?= $login_url; ?>" class="btn btn-primary btn-block" role="button">Facebook</a>
                    </div>
                    <div class="form-group" style="padding-top: 20px;">
                        <a href="<?= base_url(); ?>auth/signup" class="btn btn-default btn-block" role="button">Cadastrar</a>
                    </div>
                <?php endif; ?>
            </form>
        </div>
    </section>
</div>