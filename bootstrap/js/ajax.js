function Action(div, city, value) {
    var xmlhttp;

    if (value === "") {
        document.getElementById(div).innerHTML = "";
        return;
    }

    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else {
        // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200)
            document.getElementById(div).innerHTML = xmlhttp.responseText;
    };

    xmlhttp.open("GET", "index.php/main/execAjax?cidade=" + city + "&acao=" + value, true);
    xmlhttp.send();
}